#!/usr/bin/make -f 

# Good info at: info make "Quick Reference"
# $^ All prerequisites
# $< First prerequisity
# $@ Target

# Now we only build HTML
# Let's just use simple xslt
# If PDF and epub are needed, we can take script from debian-reference

# version of this Debian package (debian/changelog)
DEBVERSION ?= $(shell { dpkg-parsechangelog -SVersion || echo "vcs-0.0" ; })
# short date of this Debian package (debian/changelog)
DEBDATE ?= $(shell { date +'%Y-%m-%d' -d"`dpkg-parsechangelog -SDate`" || date +'(No changelog) %Y-%m-%d' ; })

XSLTPROCH  := xsltproc --xinclude $(CURDIR)/style-html.xsl
XSLTPROCT  := xsltproc --xinclude $(CURDIR)/style-txt.xsl

all: debian-java-policy/index.html debian-java-faq/index.html \
     debian-java-policy.txt debian-java-faq.txt

binary: all

%/index.html: %.xml
	mkdir -p $*
	cp debian-java.css  $*/
	cd $* ; \
	sed \
	-e "s/@@@debversion@@@/$(DEBVERSION)/g" \
	-e "s/@@@debdate@@@/$(DEBDATE)/g" \
	< $(CURDIR)/$< | \
	$(XSLTPROCH) -

%.txt: %.xml
	sed \
	-e "s/@@@debversion@@@/$(DEBVERSION)/g" \
	-e "s/@@@debdate@@@/$(DEBDATE)/g" \
	< $< | \
	$(XSLTPROCT) - | \
	LC_ALL=en_US.UTF-8 w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html \
	> $@
	
distclean: clean

clean:
	-rm -rf debian-java-policy
	-rm -rf debian-java-faq
	-rm -f  debian-java-policy.txt
	-rm -f  debian-java-faq.txt

